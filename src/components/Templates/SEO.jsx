import React from 'react';
import Helmet from 'react-helmet';

const SEO = ({title, description, image, url}) => {
    return (
        <Helmet title={title}
            htmlAttributes={{ lang: "es" }}
            meta={[
                {
                    name: `description`,
                    content: description,
                },
                {
                    property: "og:url",
                    content: url
                },
                {
                    property: "og:type",
                    content: "website"
                },
                {
                    property: "og:title",
                    content: title
                },
                {
                    property: "og:description",
                    content: description
                },
                {
                    property: "og:image",
                    content: image
                },
                {
                    property: "twitter:card",
                    content: "summary"
                },
                {
                    property: "twitter:title",
                    content: title
                },
                {
                    property: "twitter:site",
                    content: "@opencovidperu"
                },
                {
                    property: "twitter:description",
                    content: description
                },
                {
                    property: "twitter:image",
                    content: image
                }]} />
    );
}

export default SEO;