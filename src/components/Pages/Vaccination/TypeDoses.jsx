import React from "react";
import Table from "react-bootstrap/Table";

const TypeDoses = () => {
  return (
    <section className="type-doses-section">
      <Table className="table-borderless">
        <thead>
          <th>Laboratorio</th>
          <th>Total</th>
        </thead>
        <tbody>
          <tr>
            <td>Pfizer</td>
            <td>4 402 250</td>
          </tr>
          <tr>
            <td>Sinopharm</td>
            <td>1 000 000</td>
          </tr>
          <tr>
            <td>AstraZeneca</td>
            <td>787 200</td>
          </tr>
        </tbody>
      </Table>
    </section>
  );
};

export default TypeDoses;
