import React from "react";
import SEO from '../Templates/SEO';
import metaimage from '../../img/previews/opencovid-og-image_datos históricos-02.png';

const DistricPandemicSituation = () => {
  return (
    <>
      <SEO
        title="OpenCovid-Perú - Situación Distrital"
        description="El mapa mide el impacto de la pandemia teniendo en cuenta la población de esos distritos."
        image={metaimage}
        url="https://opencovid-peru.com/" />
      <main className="district-pandemic-situation">
        <iframe
          src="https://maps.amigocloud.com/api/v1/maps/1174/view"
          width="100%"
          height="600px"
        ></iframe>
      </main>
    </>
  );
};

export default DistricPandemicSituation;
